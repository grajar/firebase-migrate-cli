const colors = require("colors");
const prompt = require("../../../utils/prompt");
const firebaseUtils = require("../../../utils/firebase");
module.exports = outfitPhotoLinker;

function outfitPhotoLinker(collections) {
  return firebaseUtils
    .chooseFirebaseProject()
    .then(defaultAdmin => {
      return transformCollections(collections);
    })
    .then(transformedCollections => {
      return exportToJson(transformedCollections);
    });
}

function transformCollections(collections) {
  const collectionNames = collections.map(col => col.getName());
  const selectedIndex = collectionNames.indexOf("outfits");

  if (selectedIndex < 0) throw new Error("Outfits collection not found in selected file.");
  else return transformCollection(collections[selectedIndex]);
}

function transformCollection(collection) {
  let collectionName = collection.getName();
  let documents = collection.getDocuments();

  return transformDocuments(documents).then(docs => {
    let transformedDocuments = {};
    docs.forEach(doc => {
      let docId = Object.keys(doc)[0];
      transformedDocuments[docId] = doc[docId];
    });

    console.log(colors.green(`Transformation of "${collectionName}" successful.`));
    collection.setDocuments(transformedDocuments);
    return collection;
  });
}

function transformDocuments(documents) {
  const ids = Object.keys(documents);
  let outfitPromises = ids.map(docId => {
    let document = documents[docId];
    return reworkDocument(document, docId);
  });
  return Promise.all(outfitPromises);
}

function reworkDocument(document, docId) {
  const admin = require("firebase-admin");
  const defaultBucket = admin.storage().bucket();
  const path = `/outfits/${docId}/photos`;
  const thumbUrlPromise = defaultBucket
    .file(`${path}/${docId}-ot-570x343.jpg`)
    .getSignedUrl({
      action: "read",
      expires: "03-09-2491",
    })
    .then(signedUrls => {
      return signedUrls[0];
    });

  const coverUrlPromise = defaultBucket
    .file(`${path}/${docId}-oc-1920x680.jpg`)
    .getSignedUrl({
      action: "read",
      expires: "03-09-2491",
    })
    .then(signedUrls => {
      return signedUrls[0];
    });

  return Promise.all([thumbUrlPromise, coverUrlPromise]).then(urls => {
    const thumbUrl = urls[0];
    const coverUrl = urls[1];
    return {
      [docId]: {
        ...document,
        thumbPhoto: {
          id: `${docId}-ot-570x343`,
          path,
          links: {
            "570x343": thumbUrl,
          },
        },
        coverPhoto: {
          id: `${docId}-oc-1920x680`,
          path,
          links: {
            "1920x680": coverUrl,
          },
        },
      },
    };
  });
}

function exportToJson(collection) {
  const fileUtils = require("../../../utils/files");
  const encoders = require("../../../utils/encoders");
  console.log()

  let encodedCollections = encoders.encodeCollectionToJson(collection);
  const filename = prompt.text(`To save, enter a filename:`);
  return fileUtils.writeExportFile(filename, encodedCollections);
}
