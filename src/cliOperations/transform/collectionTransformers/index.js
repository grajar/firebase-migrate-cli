module.exports = {
  algoliaTransformer: {
    text: "Algolia: Transform collections and export JSON for Algolia upload",
    function: require("./algoliaTransformer"),
  },
  placesCleaner: {
    text: "Place Cleaner: Clean places collection and export to JSON",
    function: require("./placesCleaner"),
  },
  outfitPhotoLinker: {
    text: "Outfit photo linker: Create links to Firebase Storage photos for Outfits",
    function: require("./outfitPhotoLinker"),
  },
  bundlePhotoLinker: {
    text: "Bundle photo linker: Create links to Firebase Storage photos for Bundles",
    function: require("./bundlePhotoLinker"),
  },
};
