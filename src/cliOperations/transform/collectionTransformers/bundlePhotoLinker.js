const colors = require("colors");
const prompt = require("../../../utils/prompt");
const firebaseUtils = require("../../../utils/firebase");
module.exports = bundlePhotoLinker;

function bundlePhotoLinker(collections) {
  return firebaseUtils
    .chooseFirebaseProject()
    .then(defaultAdmin => {
      return transformCollections(collections);
    })
    .then(transformedCollections => {
      return exportToJson(transformedCollections);
    });
}

function transformCollections(collections) {
  const collectionNames = collections.map(col => col.getName());
  const selectedIndex = collectionNames.indexOf("bundles");

  if (selectedIndex < 0) throw new Error("Outfits collection not found in selected file.");
  else return transformCollection(collections[selectedIndex]);
}

function transformCollection(collection) {
  let collectionName = collection.getName();
  let documents = collection.getDocuments();

  return transformDocuments(documents).then(docs => {
    let transformedDocuments = {};
    docs.forEach(doc => {
      let docId = Object.keys(doc)[0];
      transformedDocuments[docId] = doc[docId];
    });

    console.log(colors.green(`Transformation of "${collectionName}" successful.`));
    collection.setDocuments(transformedDocuments);
    return collection;
  });
}

function transformDocuments(documents) {
  const ids = Object.keys(documents);
  let bundlePromises = ids.map(docId => {
    let document = documents[docId];
    return reworkDocument(document, docId);
  });
  return Promise.all(bundlePromises);
}

function reworkDocument(document, docId) {
  const admin = require("firebase-admin");
  const defaultBucket = admin.storage().bucket();
  const path = `/bundles/${docId}/photos`;
  const thumbUrlPromise = defaultBucket
    .file(`${path}/${docId}-bt-278x420.jpg`)
    .getSignedUrl({
      action: "read",
      expires: "03-09-2491",
    })
    .then(signedUrls => {
      return signedUrls[0];
    });

  const coverUrlPromise = defaultBucket
    .file(`${path}/${docId}-bc-1920x750.jpg`)
    .getSignedUrl({
      action: "read",
      expires: "03-09-2491",
    })
    .then(signedUrls => {
      return signedUrls[0];
    });

  return Promise.all([thumbUrlPromise, coverUrlPromise]).then(urls => {
    const thumbUrl = urls[0];
    const coverUrl = urls[1];
    return {
      [docId]: {
        ...document,
        thumbPhoto: {
          id: `${docId}-bt-278x420`,
          path,
          links: {
            "278x420": thumbUrl,
          },
        },
        coverPhoto: {
          id: `${docId}-bc-1920x750`,
          path,
          links: {
            "1920x750": coverUrl,
          },
        },
      },
    };
  });
}

function exportToJson(collection) {
  const fileUtils = require("../../../utils/files");
  const encoders = require("../../../utils/encoders");

  let encodedCollections = encoders.encodeCollectionToJson(collection);
  const filename = prompt.text(`To save, enter a filename:`);
  return fileUtils.writeExportFile(filename, encodedCollections);
}
