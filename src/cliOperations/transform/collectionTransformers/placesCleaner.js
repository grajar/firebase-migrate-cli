const colors = require("colors");
const prompt = require("../../../utils/prompt");
const randomStringGenerator = require("../../../utils/randomStringGenerator");

module.exports = placeCleaner;

function placeCleaner(collections) {
  const transformedCollections = transformCollections(collections);
  return exportToJson(transformedCollections);
}

function transformCollections(collections) {
  const collectionNames = collections.map(col => col.getName());
  const selectedIndex = collectionNames.indexOf("places");
  if (selectedIndex < 0) throw new Error("Places collection not found in selected file.");
  else return transformCollection(collections[selectedIndex]);
}

function transformCollection(collection) {
  let collectionName = collection.getName();
  let documents = collection.getDocuments();
  collection.setDocuments(transformPlaces(documents));
  console.log(colors.green(`Transformation of "${collectionName}" successful.`));
  return collection;
}

function transformPlaces(places) {
  const placeIds = Object.keys(places);
  let cleanedPlaces = {};
  placeIds.forEach(placeId => {
    let place = places[placeId];

    if (place.phone === null) delete place.phone;
    if (typeof place.lat === "string") place.lat = parseFloat(place.lat);
    if (typeof place.lng === "string") place.lng = parseFloat(place.lng);
    if (place.photos) place.photos = reworkPhotos(place.photos);
    if (place.categories) place.categories = cleanCategories(place.categories);
    cleanedPlaces[placeId] = filterPlaceFields(place);
  });
  return cleanedPlaces;
}

function reworkPhotos(photos) {
  if (!Array.isArray(photos)) {
    let reworkedPhotos = [];
    photoIds = Object.keys(photos);
    reworkedPhotos = Object.values(photos).map(photo => {
      photo.id = Object.values(photo.links)[0]
        .split("places")[1]
        .split("-")[2];
      return photo;
    });
    return reworkedPhotos;
  } else {
    return photos;
  }
}

function cleanCategories(placeCategories) {
  const allowedCategories = ["culture", "food", "drinks", "sport", "experience", "sights"];
  if (!placeCategories) return {};

  let remappedCategories = {};
  Object.keys(placeCategories).forEach(category => {
    if (!placeCategories[category]) return;
    else if (category == "sports") remappedCategories["sport"] = true;
    else if (category == "sightseeing") remappedCategories["sights"] = true;
    else if (category == "drink") remappedCategories["drinks"] = true;
    else remappedCategories[category] = true;
  });

  let finalCategories = {};
  Object.keys(remappedCategories)
    .filter(category => allowedCategories.indexOf(category) >= 0)
    .forEach(category => {
      finalCategories[category] = true;
    });
  return finalCategories;
}

function filterPlaceFields(place) {
  const allowedFields = [
    "status",
    "name",
    "pitch",
    "tags",
    "categories",
    "photos",
    "street",
    "city",
    "cc",
    "zip",
    "lat",
    "lng",
    "phone",
    "email",
    "web",
    "facebook",
    "gsm",
    "handle",
    "company",
    "rating",
    "ratingCount",
    "latestReviews",
  ];
  let filteredPlace = {};
  Object.keys(place)
    .filter(placeField => allowedFields.indexOf(placeField) >= 0)
    .forEach(placeField => {
      filteredPlace[placeField] = place[placeField];
    });

  return filteredPlace;
}

function exportToJson(collection) {
  const fileUtils = require("../../../utils/files");
  const encoders = require("../../../utils/encoders");

  let encodedCollections = encoders.encodeCollectionToJson(collection);
  const filename = prompt.text(`To save, enter a filename:`);
  return fileUtils.writeExportFile(filename, encodedCollections);
}
