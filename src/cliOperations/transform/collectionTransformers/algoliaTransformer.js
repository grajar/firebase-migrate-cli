const colors = require("colors");
const prompt = require("../../../utils/prompt");

module.exports = runAlgoliaTransformer;

function runAlgoliaTransformer(collections) {
  const transformedCollections = transformCollections(collections);
  return exportToJson(transformedCollections);
}

function transformCollections(collections) {
  const collectionNames = collections.map(col => col.getName());
  const selectedIndex = prompt.select(`Which collection do you want to operate on?`, [
    ...collectionNames,
    "All collections",
  ]);
  console.log("Performing transformation of collections for Algolia upload...");

  let transformedCollections = [];
  if (selectedIndex == collectionNames.length) {
    transformedCollections = collections.map(collection => transformCollection(collection));
  } else {
    transformedCollections = [transformCollection(collections[selectedIndex])];
  }
  return transformedCollections;
}

function transformCollection(collection) {
  let collectionName = collection.getName();
  let documents = collection.getDocuments();
  switch (collectionName) {
    case "places": {
      collection.setDocuments(transformPlaces(documents));
      console.log(colors.green(`Transformation of "${collectionName}" successful.`));
      return collection;
    }
    case "outfits": {
      collection.setDocuments(transformOutfits(documents));
      console.log(colors.green(`Transformation of "${collectionName}" successful.`));
      return collection;
    }
    case "bundles": {
      collection.setDocuments(transformBundles(documents));
      console.log(colors.green(`Transformation of "${collectionName}" successful.`));
      return collection;
    }
    default: {
      return collection;
    }
  }
}

function transformPlaces(places) {
  const placeIds = Object.keys(places);
  const arrayForAlgolia = placeIds
    .filter(placeId => {
      return places[placeId].status && places[placeId].status == 100;
    })
    .map(placeId => {
      let place = places[placeId];

      place.objectID = placeId;
      if (typeof place.lat === "string") place.lat = parseFloat(place.lat);
      if (typeof place.lng === "string") place.lng = parseFloat(place.lng);
      place._geoloc = { lat: place.lat, lng: place.lng };
      place.photo = place.photos && place.photos.length ? place.photos[0] : null;
      place.latestReviews = place.latestReviews
        ? place.latestReviews.length > 5
          ? place.latestReviews.slice(0, 5)
          : place.latestReviews
        : null;

      place.categories = Object.keys(place.categories).filter(
        category => place.categories[category]
      );
      return filterPlaceFields(place);
    });
  return arrayForAlgolia;
}

function transformOutfits(outfits) {
  const outfitIds = Object.keys(outfits);
  const arrayForAlgolia = outfitIds
    .filter(outfitId => {
      return outfits[outfitId].status && outfits[outfitId].status == 100;
    })
    .map(outfitId => {
      let outfit = outfits[outfitId];
      outfit.objectID = outfitId;
      if (typeof outfit.lat === "string") outfit.lat = parseFloat(outfit.lat);
      if (typeof outfit.lng === "string") outfit.lng = parseFloat(outfit.lng);
      outfit._geoloc = { lat: outfit.lat, lng: outfit.lng };
      outfit.categories = Object.keys(outfit.categories).filter(
        category => outfit.categories[category]
      );
      return filterOutfitFields(outfit);
    });
  return arrayForAlgolia;
}

function transformBundles(bundles) {
  const bundleIds = Object.keys(bundles);
  const arrayForAlgolia = bundleIds
    .filter(bundleId => {
      return bundles[bundleId].status && bundles[bundleId].status == 100;
    })
    .map(bundleId => {
      let bundle = bundles[bundleId];
      bundle.objectID = bundleId;
      if (typeof bundle.lat === "string") bundle.lat = parseFloat(bundle.lat);
      if (typeof bundle.lng === "string") bundle.lng = parseFloat(bundle.lng);
      bundle._geoloc = { lat: bundle.lat, lng: bundle.lng };
      bundle.categories = Object.keys(bundle.categories).filter(
        category => bundle.categories[category]
      );
      return filterBundleFields(bundle);
    });
  return arrayForAlgolia;
}

function filterPlaceFields(place) {
  const allowedFields = [
    "objectID",
    "name",
    "pitch",
    "tags",
    "categories",
    "_geoloc",
    "photo",
    "rating",
    "latestReviews",
  ];
  let filteredPlace = {};
  Object.keys(place)
    .filter(placeField => allowedFields.indexOf(placeField) >= 0)
    .forEach(placeField => {
      filteredPlace[placeField] = place[placeField];
    });

  return filteredPlace;
}

function filterBundleFields(bundle) {
  const allowedFields = [
    "objectID",
    "title",
    "subtitle",
    "text",
    "categories",
    "_geoloc",
    "coverPhoto",
    "thumbPhoto",
    "places",
  ];
  let filteredBundle = {};
  Object.keys(bundle)
    .filter(bundleField => allowedFields.indexOf(bundleField) >= 0)
    .forEach(bundleField => {
      filteredBundle[bundleField] = bundle[bundleField];
    });

  return filteredBundle;
}

function filterOutfitFields(outfit) {
  const allowedFields = [
    "objectID",
    "title",
    "subtitle",
    "categories",
    "_geoloc",
    "coverPhoto",
    "thumbPhoto",
    "morning",
    "day",
    "evening",
  ];
  let filteredOutfit = {};
  Object.keys(outfit)
    .filter(outfitField => allowedFields.indexOf(outfitField) >= 0)
    .forEach(outfitField => {
      filteredOutfit[outfitField] = outfit[outfitField];
    });
  return filteredOutfit;
}

function exportToJson(collections) {
  const fileUtils = require("../../../utils/files");
  const isMultiExport = collections.length > 1;

  if (isMultiExport) {
    const folderName = prompt.text(`Enter a folder name to save the exports in: `);
    let writePromises = collections.map(collection =>
      fileUtils.writeExportFile(`${folderName}/${collection.getName()}.json`, [
        ...collection.getDocuments(),
      ])
    );
    return Promise.all(writePromises);
  } else {
    const filename = prompt.text(`To save, enter a filename:`);
    let algoliaReadyDocuments = Object.values(collections[0].getDocuments());
    return fileUtils.writeExportFile(filename, algoliaReadyDocuments);
  }
}
