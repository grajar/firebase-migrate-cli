const prompt = require("../../utils/prompt");
const colors = require("colors");

module.exports = runTransformingCLI;

function runTransformingCLI() {
  intro();
  const collections = readDumpFile();
  const transformer = chooseTransfomer();
  return transformer(collections);
}

function intro() {
  console.log(
    "Parsing selected. You will be prompted for a database dump file. Expected file format of database dumps:" +
      `
  {
      "places": {                     // collection name
          "00AJNAPsVnxZEZfM7fKu": {   // document id
              "name": "Starbucks",    // document data
          }
          ,...`
  );
}

function readDumpFile() {
  const encodedCollections = prompt.file("Enter database dump filename and relative path:", "json");
  if (encodedCollections === undefined) {
    return readDumpFile();
  }
  let parsers = require("../../utils/encoders");
  return parsers.decodeJsonFormat(encodedCollections);
}

function chooseTransfomer() {
  const transfomers = require("./collectionTransformers");
  const transfomerIds = Object.keys(transfomers);
  const transfomerNames = transfomerIds.map(transformerId => transfomers[transformerId].text);

  const selectedIndex = prompt.select(`Please choose a transfomer:`, transfomerNames);
  return transfomers[transfomerIds[selectedIndex]].function;
}
